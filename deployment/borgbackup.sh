#!/bin/ksh
. /etc/borg-env
export BORG_REPO=nathan@nephilim:repositories-borg/ticketfrei
export BORG_RSH="ssh \
-o TCPKeepAlive=no \
-o ServerAliveInterval=15 \
-o ServerAliveCountMax=10 \
-o Compression=no"

rcctl stop backend_daemon
rcctl stop frontend_daemon
/usr/local/bin/borg create --stats ::'backup{now:%Y%m%d-%H%M}' /srv/ticketfrei /var/ticketfrei /etc
rcctl start backend_daemon
rcctl start frontend_daemon

